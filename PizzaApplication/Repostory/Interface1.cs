﻿using PizzaApplication.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaApplication.Repostory
{
    enum ServiceCharge
    {
        CGST = 9,
        SGST = 9
    }

    interface ICustomerModule //customer functionality
    {
        bool OrderPizza(int pizzaId);
        bool CancelPizza();
        List<Pizza> GetAllPizza();
        double GetBill();

    }

    interface IPizzaModule //pizza functionality 
    {
        bool AddNewPizza(Pizza add_pizza);
        bool DeletePizza(int del_pizza);
        bool UpdatePizzaName(int update_id, string? update_name);
        bool UpdatePizzaPrice(int id, int price);
        List<Pizza> GetAllPizza();
        bool GetIdValid(int update_id);
    }


}
