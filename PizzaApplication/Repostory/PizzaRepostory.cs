﻿using PizzaApplication.Database;
using PizzaApplication.Model;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaApplication.Repostory
{
    internal class PizzaRepostory:ICustomerModule,IPizzaModule
    {
        private static SqlConnection conn = DBConnection.getConnection();
        private static SqlCommand cmd;

        List<Pizza> AllPizza = null;
        int totalPizza = 6;

        List<int> order_pizza = new List<int>();
        int orderedPizzaByUser = 0;

        public PizzaRepostory() //non-parameterise constructor or default constructor
        {
            AllPizza = DBConnection.getAllPizza();
            foreach (Pizza p in AllPizza)
            {
                p.ToString();
            }
        }

        public List<Pizza> GetAllPizza() // Available in Customer as well as in pizza module 
        {
            return AllPizza;
        }

        public bool OrderPizza(int pizzaId) //Available in Customer
        {
            foreach (Pizza pizza in AllPizza)
            {
                if (pizza.pizzaId == pizzaId)
                {
                    order_pizza.Add(pizzaId);
                    orderedPizzaByUser++;
                    return true;
                }
            }
            return false;
        }

        public bool CancelPizza()  //Available in Customer
        {
            if (orderedPizzaByUser > 0)  //cross check
            {
                order_pizza = null;
                order_pizza = new List<int>();
                return true;
            }
            return false;
        }

        public bool GetIdValid(int pizzaId)
        {
            foreach (Pizza pizza in AllPizza)
            {
                if (pizza.equals(pizzaId))
                {
                    return true;
                }
            }
            return false;
        }


        public double GetBill() //Available in Customer
        {
            double sum = 0;
            foreach (Pizza pizza in AllPizza)
            {
                if (order_pizza.Contains(pizza.pizzaId))
                {
                    sum = sum + pizza.pizzaPrice;
                    order_pizza.Remove(pizza.pizzaId);
                }
            }
            sum = sum + (sum * ((int)ServiceCharge.CGST / 100));  //Enum CGST -> 9%
            sum = sum + (sum * ((int)ServiceCharge.SGST / 100));  //Enum SGST -> 9%
            return sum;
        }

        public bool AddNewPizza(Pizza addPizza)
        {
            conn.Open();
            cmd.CommandText = $"insert into products values({addPizza.pizzaId},'{addPizza.pizzaName}',{addPizza.pizzaPrice})";
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
            
            return true;
        }

        public bool DeletePizza(int delPizza)
        {
            conn.Open();
            cmd.CommandText = $"delete from products where id={delPizza}";
            cmd.Connection = conn;
            return 1 == cmd.ExecuteNonQuery();
        }

        public bool UpdatePizzaPrice(int pizzaId, int pizzaPrice)
        {
            conn.Open();
            cmd.CommandText = $"update products set price={pizzaPrice} where id={pizzaId}";
            cmd.Connection = conn;
            return 1 == cmd.ExecuteNonQuery();
        }

        public bool UpdatePizzaName(int pizzaId, string? newName)
        {
            conn.Open();
            cmd.CommandText = $"update products set pizzaName={newName} where id={pizzaId}";
            cmd.Connection = conn;
            return 1 == cmd.ExecuteNonQuery();
        }


    }
}
