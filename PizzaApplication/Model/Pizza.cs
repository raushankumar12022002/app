﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaApplication.Model
{
    internal class Pizza
    {
        public int pizzaId { get; set; }
        public string? pizzaName { get; set; }
        public int pizzaPrice { get; set; }

        public Pizza(int pizzaId, string pizzaName, int pizzaPrice) //constructor
        {
            this.pizzaId = pizzaId;
            this.pizzaName = pizzaName;
            this.pizzaPrice = pizzaPrice;
        }
        public override string ToString()//To check data of the current obj
        {
            return ($"PizzaId::{pizzaId}\tPizzaName::{pizzaName}\tPizzaPrice{pizzaPrice}/-");
        }

        public bool equals(int pizzaid)
        {
            if (this.pizzaId == pizzaid)
                return true;
            return false;
        }



    }
}
