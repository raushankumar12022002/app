﻿using PizzaApplication.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaApplication.Database
{
    internal class DBConnection
    {
        private static SqlConnection conn;
        private static SqlCommand cmd = new SqlCommand();
        public static List<Pizza> allPr;



        public static SqlConnection getConnection()
        {
            conn = new SqlConnection("Server=RAUSHANKUMAR;Database=Pizza_Db;Trusted_Connection=True");
            return conn;
        }

        public static List<Pizza> getAllPizza()
        {
            getConnection();
            allPr = new List<Pizza>();
            cmd.CommandText = "select * from product1";
            cmd.Connection = conn;
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                allPr.Add(new Pizza((int)reader["PizzaId"], (string)reader["PizzaName"], (int)reader["PizzaPrice"]));
            }
            conn.Close();
            return allPr;
        }



    }
}
